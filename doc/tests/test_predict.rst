Matching color curves using spline prediction
==============================================

I created this model because in pixel art, you are often limited in
number of colors available. Therefore it makes sense to, from the colors
available, choose the 'best-fitting' sequences of colors.

The spline prediction routines can predict a good next choice for any
multivariate data set (eg. RGB colors), and return a tuple ((error,
choice), (error,choice)) for each requested prediction.

>>> from ion.n.predict import predict, insert


predict () accepts a data set [NITEMS][NVARIABLES], an ordered list of
choices, and one or more requested x-values. values are evenly spaced on
x axis so eg. 1.5 is guaranteed to be halfway between the second item
(#1) and the third (#2)

predict ()  is oriented towards incremental, interactive choosing --
using predict() to offer the best choices quickest.


>>> dataset = [[0,0, 128], [0,192, 128], [0,192,255], [0,255,255], [255,255,255]]
>>> choices = [0, 2]

With a very small dataset, polynomials of order 2 or 3 are used instead of
splines.

>>> wantedpoints = [.5, 2]
>>> chosen = predict (dataset, choices, wantedpoints)
>>> chosen
(((0,96,0), 1), ((0, -33,-63), 3))

insert is a simple helper which inserts the returned choices at
appropriate places in the choices list.

>>> insert (choices, wantedpoints, chosen)
>>> choices
[0, 1, 2, 3]

where spline interpolation order (either 2 or 3) = len(choices) - 1, a
single polynomial has identical effect to a spline.

>>> wantedpoints = [4]
>>> chosen = predict (dataset, chosen, wantedpoints, order = 3)
(((255,-381,381), 4),)

