>>> from ion.n.random import *

cumulative_to_simple_weights is a generalization of the OHRRPGCE item reward algorithym.
In the OHRRPGCE, an enemy may yield one of two items (or nothing), when defeated.
These are the item, and the rare item. Each has an associated percentage.
The algorithym is quite simple, and is implemented in the function gotItem()
along these lines:

IF percent (item%) THEN give item ELSE IF percent (rareitem%) THEN give rareitem ELSE give nothing.

Thus, choosing item% = 50 and rareitem% = 50 gives

   50% chance of item, 25% chance of raretem, 25% chance of nothing.
      
The principle of taking subsequent percentages as a percentage of the remaining part of 100% is used.
Thus, you may use 100% as a percentage; but only once.
[100, foo, bar] # foo and bar don't matter (the chance of them is 0%, regardless what values you assign)
[foo,bar, 100]  # if foo-item or bar-item aren't chosen, then 100-item is guaranteed.

cumulative_to_simple_weights converts the cumulative-percentages format into straight weights.

>>> cumulative_to_simple_weights ([50.0,50.0])
[50.0, 25.0, 25.0]

The final item (chance of Nothing) is omitted if it is 0%:

>>> cumulative_to_simple_weights ([50.0, 100.0])
[50.0, 50.0]

In this case the last item is effectively the default (rather than Nothing)
### more here on aW()


dispense is used for situations where you wish to dispense items randomly from a given 'stock' - thus, it is guaranteed that until the 'box' is emptied, no more of an item may be dispensed than the number initially in it.

>>> box = ['gold', 'malachite', 'topaz']

You need a status sequence, where temporary status information can be kept. It needs only to
support seq.extend() and seq.append()

>>> status = []

dispense is similar to boxfill; dispense is forced to dispense only items never dispensed before, where boxfill simply weights probability toward dispensing fresh items.

>>> item = dispense (box, status)
>>> item in box
True

If you try to take out more items than are currently available, Nones are returned in their place.

>>> items = dispense (box, status, n = 3) 
>>> items.count(None)
1

>>> items[-1] == None
True

After emptying, the box starts out full again.

>>> item = dispense (box, status)
>>> item in box
True
>>> item != None
True


XXX gotitem; samples; choices; boxfill

