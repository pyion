
.. currentmodule:: ion

.. _misc::

Miscellaneous
===============

.. autosummary::
    :toctree: generated/
    
    read_everything
    read_everyline
    order
    tupled
    piz
    cycle
    setattrs
    notional_nbits
    min_nbits
    
.. autofunction:: ion.setattrs

    .. todo:: 
    
        check attributes are python-accessible (dummy todo item)
    



    