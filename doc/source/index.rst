.. ion documentation master file, created by sphinx-quickstart on Thu Nov  6 23:30:08 2008.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ion's documentation!
===============================

Contents:

.. toctree::
   :maxdepth: 2

   io
   misc
   randomness
   sorting
   

.. index:: convenience
Indices and tables
==================

* :ref:`todo`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



