from setuptools import setup, find_packages
import sys, os

version = '0.02'

setup(name='pyion',
      version=version,
      description="Utility library. IO, maths, randomizations, misc.",
      long_description="""File handling, maths, randomization, miscellaneous.      
INCLUDING:
  * general maths (most functions support both scalars and list/array-likes)
  ** factorials
  ** fast sum of series 1,2,3,4,5 ... N
  ** number of sequences of N size taken from a set of K size
  ** number of places a sequence of length N can be subdivided
  ** round N to nearest power of B
  ** convert a power of B, N, to an exponent of B, such that B**E == N
  ** find common denominator
  ** find all integers that divide evenly into N
  ** linear/spline interpolation of a 1d series of values
  ** binary representation of a number
  * spatial maths
  ** subclassable Grid type
  ** grid snapping and 'is-in-cell'
  ** polar/cartesian transform
  ** polar distance
  ** calculate triple of weights based on position in triangle
  * randomization
  ** return true P% of the time
  ** weighted sampling from a set of items
  ** fill weighted boxes in a roughly even way
  ** randomly dispense contents of a box
  ** convert nested percentage weights to flat 0..1 weights
  ** dice roll (single, and iterative/infinite)
  ** chain iterator, which yields values while percentage chance of success
     rolls succeed, and stops as soon as they fail.
     Like OHRRPGCE 'chain attack/ chain %' option
  * misc
  ** find numeric limits of data types (builtin types or simple numpy types)
  ** sort sequence according to template
  ** reverse of zip (flat sequence a,b,c,a,b,c,a,b,c -> a,b,c tuples)
  ** easily read every line or every byte of a file
  ** calculate minimum number of bits required to store a given integer.
  ** quickly set multiple attributes on an object
  ** Metadata class for easy management of metadata,
     with yaml serialization support.

also available through git. run 'git clone git://repo.or.cz/pyion.git' to grab a copy :)      
  """,
      classifiers=['Development Status :: 4 - Beta',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: GNU General Public License (GPL)',
                   'Programming Language :: Python :: 2.5',
                   'Topic :: Software Development :: Libraries :: Python Modules'],
                   # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='file math random miscellaneous',
      author='David Gowers',
      author_email='00ai99@gmail.com',
      url='http://repo.or.cz/w/pyion.git',
      download_url='http://repo.or.cz/pyion.git',
      license='GPLv3',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests', 'doc']),
      include_package_data=True,
      zip_safe=True,
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
