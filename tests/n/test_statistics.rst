>>> from ion.n.statistics import *

Currently only the 'x.categories' format is supported.
test_statistics holds some interesting stuff for this.

>>> from test_statistics import add, items, categorylist
>>> categorylist
['armor', 'weapon', 'gloves', 'shield', 'blunt']

So lets add some items.

>>> add ('leather gloves: armor gloves','gauntlets: armor gloves',)
>>> add ('buckler: armor shield','small shield: armor shield','large wooden shield: armor shield')
>>> add ('plate shield: armor shield','tower shield: armor shield')
>>> add ('club: weapon blunt','mace: weapon blunt')

Let's see what we have, now:

>>> items
[T ('leather gloves:armor gloves'), T ('gauntlets:armor gloves'), T ('buckler:armor shield'), T ('small shield:armor shield'), T ('large wooden shield:armor shield'), T ('plate shield:armor shield'), T ('tower shield:armor shield'), T ('club:weapon blunt'), T ('mace:weapon blunt')]

>>> items[0].categories
(0, 2)

Now try to generate a table

>>> tab = aproposTable (items)

(doctest can't be relied upon in the matter of the ordering of dictionaries,
so I convert tab to a rather lisp-ish looking list.)
>>> _tab = tab.items()
>>> _tab.sort(key = lambda v:v[0].id)
>>> _tab
[(T ('leather gloves:armor gloves'), (T ('leather gloves:armor gloves'), 1, T ('gauntlets:armor gloves'), 2, T ('buckler:armor shield'), 1, T ('small shield:armor shield'), 1, T ('large wooden shield:armor shield'), 1, T ('plate shield:armor shield'), 1, T ('tower shield:armor shield'), 1)), (T ('gauntlets:armor gloves'), T ('leather gloves:armor gloves')), (T ('buckler:armor shield'), T ('small shield:armor shield')), (T ('small shield:armor shield'), (T ('small shield:armor shield'), 1, T ('large wooden shield:armor shield'), 2, T ('plate shield:armor shield'), 2, T ('tower shield:armor shield'), 2)), (T ('large wooden shield:armor shield'), T ('small shield:armor shield')), (T ('plate shield:armor shield'), T ('small shield:armor shield')), (T ('tower shield:armor shield'), T ('small shield:armor shield')), (T ('club:weapon blunt'), (T ('club:weapon blunt'), 1, T ('mace:weapon blunt'), 2)), (T ('mace:weapon blunt'), T ('club:weapon blunt'))]

Now I'll look something up: club vs mace

>>> items[-2:]
[T ('club:weapon blunt'), T ('mace:weapon blunt')]
>>> aproposness (tab, items[-1], items[-2])
2

Gloves have little in common with a mace.

>>> aproposness (tab, items[-1], items[0])
0

What *is* apropos to gloves, then?

>>> aproposChoices (tab, items[0], items, 1)
[T ('leather gloves:armor gloves'), T ('gauntlets:armor gloves'), T ('buckler:armor shield'), T ('small shield:armor shield'), T ('large wooden shield:armor shield'), T ('plate shield:armor shield'), T ('tower shield:armor shield')]

More strictly apropos?

>>> aproposChoices (tab, items[0], items, 2)
[T ('gauntlets:armor gloves')]