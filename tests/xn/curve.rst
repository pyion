>>> import ion.xn.curve as amc
>>> amc._tile ([0,1,2,3], [40, 64, -1, 4], 1, (4,6))
([0, 1, 2, 3, 4, 5, 6], [40, 64, -1, 4, 40, 64, -1])

>>> amc._tile ([0,1,2,3], [40, 64, -1, 4], -1, (-3, -1))
([-3, -2, -1, 0, 1, 2, 3], [64, -1, 4, 40, 64, -1, 4])

>>> amc._extend ([0,1,2,3], [40, 64, -1, 4], 1, (4,6))
[0,1,2,3,4,5,6], [40, 64, -1, 4, 44, 68, 3]

>>> amc._extend ([0,1,2,3], [40, 64, -1, 4], -1, (-3, -1))
[-3, -2, -1, 0, 1, 2, 3], [24, -41, -36, 40, 64, -1, 4]


# XXX instead of expanding the spline, wrap the requested X values and add an appropriate offset.
# eg for tile with a spline ranging 0..n values of X, you would just return spl(x % max(spl))
# for min(spl) != 0, normalization must also be performed : spl (((x - min(spl)) % max(spl)) + min(spl))

>>> ms = amc.multiSpline ([0,.25,.50,.75,1.], [[0, 0, 0], [.4, .05, .1], [.5, .25, .3], 
... [1., 0.95, .6], [1., 1., 1.]], k = 3)

>>> import numpy
>>> numpy.round(ms(numpy.arange(0,1.01, 1./8)), 2)
array([[-0.  , -0.  ,  0.  ],
       [ 0.31,  0.05,  0.04],
       [ 0.4 ,  0.05,  0.1 ],
       [ 0.42,  0.09,  0.19],
       [ 0.5 ,  0.25,  0.3 ],
       [ 0.73,  0.59,  0.44],
       [ 1.  ,  0.95,  0.6 ],
       [ 1.14,  1.15,  0.79],
       [ 1.  ,  1.  ,  1.  ]])

Notice the overflow above: >1 is not legal.
fixing that:

>>> ms = amc.multiSpline ([0,.25,.50,.75,1.], [[0, 0, 0], [.4, .05, .1], [.5, .25, .3], 
... [1., 0.95, .6], [1., 1., 1.]], k = 3, subdiv = True, protected = ( (0,1), (1,2), (2,3),))

>>> numpy.round(ms(numpy.arange(0,1.01, 1./8)), 2)
array([[-0.  , -0.  ,  0.  ],
       [ 0.31,  0.05,  0.04],
       [ 0.4 ,  0.05,  0.1 ],
       [ 0.42,  0.09,  0.19],
       [ 0.5 ,  0.25,  0.3 ],
       [ 0.73,  0.59,  0.44],
       [ 1.  ,  0.95,  0.6 ],
       [ 1.  ,  0.97,  0.79],
       [ 1.  ,  1.  ,  1.  ]])